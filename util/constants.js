const EDUCATION_SECTION_TITLES = ['education', 'academic history'];
const SKILL_SECTION_TITLES = ['skills', 'technical skills', 'programming skills', 'specialized skills'];
const OTHER_SECTION_TITLES = ['internship', 'project experience', 'coursework', 'experience', 'projects',
    'capstone and academic projects', 'internships', 'certifications', 'objectives',
    'individual projects', 'course projects', 'work experience', 'professional experience', 'relevant courses',
    'activities', 'graduate courses', 'technical and computational skills', 'internship experiences',
    'publications', 'notables', 'extra curricular activities', 'contact', 'leadership', 'awards', 'research experience',
    'relevant coursework', 'leadership and fellowship', 'selected projects', 'academic projects', 'leadership & achievements',
    'certifications/publications', 'additional information', 
];
const SECTION_TITLES = EDUCATION_SECTION_TITLES.concat(SKILL_SECTION_TITLES)
    .concat(OTHER_SECTION_TITLES);
const GOOGLE_VISION_API_KEY = 'AIzaSyAjssR5yVkZGnaLVQ9ImaIgZzu_9eTKVmo';

module.exports = {
    EDUCATION_SECTION_TITLES,
    SKILL_SECTION_TITLES,
    SECTION_TITLES,
    GOOGLE_VISION_API_KEY
};