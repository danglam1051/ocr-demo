const request = require('request');
const constants = require('./constants');

const callOcrApi = (base64Content) => {
    return new Promise((resolve) => {
        request.post(
            'https://vision.googleapis.com/v1/images:annotate?key=' + constants.GOOGLE_VISION_API_KEY, {
                json: {
                    "requests": [{
                        "image": {
                            "content": base64Content
                        },
                        "features": [{
                            "type": "DOCUMENT_TEXT_DETECTION"
                        }]
                    }]
                }
            },
            function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    resolve(body);
                } else {
                    console.error(error);
                }
            }
        );
    });
};

module.exports.callOcrApi = callOcrApi;