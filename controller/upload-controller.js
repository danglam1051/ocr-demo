const multer = require("multer");
const fs = require("fs");

const uploader = multer({
    dest: "./uploads/"
});

const imgToBase64 = (path) => {
    const base64ImageContent = new Buffer(fs.readFileSync(path)).toString("base64");

    fs.unlink(path, () => {});

    return base64ImageContent;
}

module.exports.uploader = uploader;
module.exports.imgToBase64 = imgToBase64;