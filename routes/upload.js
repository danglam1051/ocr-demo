const express = require('express');
const router = express.Router();
const uploadController = require('../controller/upload-controller');
const visionApi = require('../util/vision-api');
const textProcessor = require('../util/text-processor');

router.post("/", uploadController.uploader.single("cv"), async (req, res) => {
  const base64ImageContent = uploadController.imgToBase64(req.file.path);

  const apiResponse = await visionApi.callOcrApi(base64ImageContent);

  const textBlocks = textProcessor.splitTextToBlocks(apiResponse);

  const fullName = textProcessor.getName(textBlocks).split(' ');
  const firstName = fullName[0];
  const lastName = fullName[fullName.length - 1];
  const email = textProcessor.getEmail(textBlocks);
  const education = textProcessor.getEducation(textBlocks);
  const skills = textProcessor.getSkills(textBlocks);

  res.render('result', {
    firstName: firstName,
    lastName: lastName,
    email: email,
    education: education,
    skills: skills
  });
});

module.exports = router;