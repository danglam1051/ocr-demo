const constants = require('./constants');

const charsInUrl = new RegExp(/[.|@]/gi);
const emailRegex = new RegExp(/[a-z0-9\._%+!$&*=^|~#%'`?{}/\-]+@([a-z0-9\-]+\.){1,}([a-z]{2,16})/);

// Split full text into blocks of information
const splitTextToBlocks = (data) => {
    const blocks = [];
    blocks.push([]);
    let blockIndex = 0;

    const fullText = data.responses[0].textAnnotations[0].description;

    fullText.split("\n").forEach((text) => {
        const temp = text.replace(/[^\w\s!?]/g, '').trim().toLowerCase();
        if (constants.SECTION_TITLES.indexOf(temp) > -1) {
            text = temp;
            blocks.push([]);
            blockIndex++;
        }
        blocks[blockIndex].push(text);
    });

    return blocks;
};

const getName = (blocks) => {
    const infoBlock = blocks[0];
    let fullName;

    for (const paragraph of infoBlock) {
        if (charsInUrl.test(paragraph)) {
            continue;
        }
        fullName = paragraph;
        break;
    }

    return fullName;
};

const getEmail = (blocks) => {
    const infoBlock = blocks[0];
    let email;

    for (let paragraph of infoBlock) {
        if (emailRegex.test(paragraph)) {
            paragraph = paragraph.split(/[\s/]+/);
            for (const word of paragraph) {
                if (emailRegex.test(word)) {
                    email = word;
                }
            }
            break;
        }
    }

    return email;
}

const getEducation = (blocks) => {
    let educationBlock;
    blocks.forEach((block) => {
        if (constants.EDUCATION_SECTION_TITLES.indexOf(block[0].toLowerCase()) > -1) {
            educationBlock = block.slice();
        }
    });

    if (!educationBlock) return 'Not found';

    return '- ' + educationBlock.slice(1).join('\n- ');
};

const getSkills = (blocks) => {
    let skillBlock;
    blocks.forEach((block) => {
        if (constants.SKILL_SECTION_TITLES.indexOf(block[0].toLowerCase()) > -1) {
            skillBlock = block.slice();
        }
    });

    if (!skillBlock) return 'Not found';

    return '- ' + skillBlock.slice(1).join('\n- ');
}

module.exports = {
    splitTextToBlocks,
    getName,
    getEmail,
    getEducation,
    getSkills
};